<?php


namespace app\controllers;
use app\models\OrderItem;
use app\models\Orders;
use app\models\Products;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\HttpException;


class CartController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionAddCart(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->get('id');
            $son = Yii::$app->request->get('son');
            $products = new Products();
            $model = Products::findOne($id);
            debug($son);
            if(!empty($son)){
                $products->saveDb($model, $son);
                $products->addToCart($model, $son);
            }
            else{
                $products->saveDb($model);
                $products->addToCart($model);
            }
            $this->layout = false;
            return $this->render('cart', compact('model'));
        }
        else{
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionRemove(){
        $product = new Products();
        $product_id = Yii::$app->request->get('id');
        $order = Orders::findOne([
            'user_id' => Yii::$app->user->identity->id,
            'product_id' => $product_id
        ]);
        $product->DelItems($product_id);
        $this->layout = false;
        $model = Orders::find()->all();
        if($order->delete()){
            Yii::$app->session->setFlash('soni', $_SESSION['cart.son']);
            return $this->render('cart', compact('model'));
        }
        else{
            return false;
        }
    }

    public function actionWishList(){
        $model = Orders::find()
        ->asArray()
        ->where(['user_id' => Yii::$app->user->identity->id])
        ->all();
        return $this->render('carts', compact('model'));
    }

    public function actionDelItems(){
        $id = Yii::$app->request->get('id');
        $order = Orders::findOne($id);
        $product = new Products();
        $product->DelItems($order->product_id);
        if($order->delete()){
            Yii::$app->session->setFlash('success', "Ma'lumot o'chirildi!");
            return $this->redirect(['cart/wish-list']);
        }
        else{
            Yii::$app->session->setFlash('success', "Ma'lumot o'chirilmadi!");
            return $this->redirect(['cart/wish-list']);
        }
    }

    public function actionOformit(){
        $id = Yii::$app->request->get('id');
        $order = Orders::findOne([
            'id' => $id
        ]);
        $product = Products::findOne([
            'id' => $order->product_id
        ]);

        $model = new OrderItem();

        if($model->load(Yii::$app->request->post())){
            Yii::$app->mailer->compose('order', ['session' => $_SESSION['cart']])
                ->setFrom(['admin@mail.ru' => 'Umidjon'])
                ->setTo($model->email)
                ->setSubject($model->product_name)
                ->send();
            $model->user_id = Yii::$app->user->identity->id;
            $model->product_id = $product->id;
            $model->status = 'active';
            $row = OrderItem::findOne([
                'user_id' => Yii::$app->user->identity->id,
                'product_id' => $product->id
            ]);
            if(!empty($row)){
                $row->soni += $model->soni;
                if($row->save()){
                    Yii::$app->session->setFlash('success', "Buyurtmangiz yuborildi!");
                    $product->DelItems($order->product_id);
                    $order->delete();
                    return $this->redirect(['cart/wish-list']);
                }
                else{
                    Yii::$app->session->setFlash('error', "Ma'lumotlar yuborilmadi!");
                    return $this->refresh();
                }
            }
            else{
                if($model->save()){
                    Yii::$app->session->setFlash('success', "Buyurtmangiz yuborildi!");
                    $product->DelItems($order->product_id);
                    $order->delete();
                    return $this->redirect(['cart/wish-list']);
                }
                else{
                    Yii::$app->session->setFlash('error', "Ma'lumotlar yuborilmadi!");
                    return $this->refresh();
                }
            }
        }
        return $this->render('ofor', compact('order', 'product', 'model'));
    }

    public function actionSaveAll(){
        $user_id = Yii::$app->user->identity->id;
        $product = new Products();
        $orders = Orders::find()
            ->asArray()
            ->where(['user_id' => $user_id])
            ->all();
        $model = new OrderItem();
        $array = [];

        if($model->load(Yii::$app->request->post())){
            $n = $model->fullname;
            $email = $model->email;
            $address = $model->address;
            $phone = $model->phone;
            foreach ($orders as $k => $r){
                $del = Orders::findOne($r['id']);
                $row = OrderItem::findOne([
                    'user_id' => $user_id,
                    'product_id' => $r['product_id'],
                    'status' => 'active'
                ]);
                if(!empty($row)){
                    $row->soni += $r['son'];
                    $row->save(false);
                }
                else{
                    $model->fullname = $n;
                    $model->email = $email;
                    $model->phone = $phone;
                    $model->address = $address;
                    $model->user_id = $user_id;
                    $model->product_name = $r['name'];
                    $model->product_id = $r['product_id'];
                    $model->sum = $r['sum'];
                    $model->soni = $r['son'];
                    $model->images = $r['images'];
                    $model->status = 'active';
                    if($model->save()){
                        $model = new OrderItem();
                    }
                }
                $product->DelItems($del->product_id);
                $del->delete();
            }
            Yii::$app->session->setFlash('success', "Buyurtmangiz qabul qilindi!");
            return $this->redirect(['main/index']);
        }
        return $this->render('save', compact('model', 'orders'));
    }
}