<?php


namespace app\controllers;
use app\models\OrderItem;
use app\models\Orders;
use yii\web\Controller;
use Yii;


class AppController extends Controller
{
    public $count;
    public $model = [];
    public $order_count;
    public $order = [];
    public $order_sum;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        if(!Yii::$app->user->isGuest){
            $order_item = OrderItem::find()
                ->asArray()
                ->where(['email' => Yii::$app->user->identity->email])
                ->all();
            if(!empty($order_item)){
                foreach ($order_item as $r){
                    $this->order_count += $r['soni'];
                    $this->order_sum += $r['soni'] * $r['sum'];
                }
                $this->order = $order_item;
            }
            else{
                $this->order_count = 0;
            }

            $orders = Orders::find()->asArray()->where(['user_id' => Yii::$app->user->identity->id])->all();
            if(!empty($orders)){
                foreach ($orders as $r){
                    $this->count += $r['son'];
                }
                $this->model = $orders;
            }
            else{
                $this->count = 0;
            }


        }
    }

    protected function setMeta($title = null, $keywords = null, $description = null){
        $this->view->title = $title;
        $this->view->registerMetaTag([
           'name' => 'keywords',
           'content' => "$keywords"
        ]);
        $this->view->registerMetaTag([
           'name' => 'description',
           'content' => "$description"
        ]);
    }
}