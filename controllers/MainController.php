<?php

namespace app\controllers;
use app\models\Category;
use app\models\Orders;
use app\models\Products;
use app\models\Signup;
use app\models\Users;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\Pagination;
use yii\web\HttpException;
use app\models\LoginForm;


class MainController extends AppController
{

    public function actionIndex(){
        $this->setMeta('Xarid savatcha');
        $model = Products::find()
            ->asArray()
            ->where(['new' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->all();
        return $this->render('index', compact('model'));
    }

    public function actionCategory(){
        $id = Yii::$app->request->get('id');
        $model = Category::findOne(['id' => $id]);
        $products = Products::find()
        ->asArray()
        ->where(['category_id' => $model->id]);
        $page = new Pagination([
            'totalCount' => $products->count(),
            'pageSize' => 9,
            'forcePageParam' => false,
            'pageSizeParam' => false
        ]);
        $products = $products
            ->offset($page->offset)
            ->limit($page->limit)
            ->all();
        $this->setMeta($model->name, $model->keywords, $model->description);
        return $this->render('category', compact('page','model', 'products'));
    }
    public function actionProducts(){
        $id = Yii::$app->request->get('id');
        $model = Products::findOne($id);
        $this->setMeta($model->name, $model->keywords, $model->description);
        return $this->render('products', compact('model'));
    }

    // ajax
    public function actionDataOne(){
        $id = Yii::$app->request->get('id');
        $model = Products::findOne($id);
        if(Yii::$app->request->isAjax){
            $this->layout = false;
            return $this->renderAjax('data_one', compact('model'));
        }
        return $this->render('data_one', compact('model'));
    }

    public function actionSignup(){
        $model = new Signup();
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post())){
            $model->status = 'active';
            if($model->signup()){
                return $this->redirect(['main/login']);
            }
            else{
                Yii::$app->session->setFlash('error', 'Malumotlar saqlanmadi!');
                return $this->refresh();
            }
        }
        return $this->render('signup', compact('model'));
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}