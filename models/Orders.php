<?php

namespace app\models;
use Yii;

class Orders extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%orders}}';
    }

    public function rules()
    {
        return [
            [['user_id', 'product_id', 'name', 'sum', 'son', 'images', 'status'], 'required'],
            [['user_id', 'product_id', 'name', 'son'], 'integer'],
            [['sum'], 'number'],
            [['status'], 'string'],
            [['images'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'name' => Yii::t('app', 'Name'),
            'sum' => Yii::t('app', 'Sum'),
            'son' => Yii::t('app', 'Son'),
            'images' => Yii::t('app', 'Images'),
            'status' => Yii::t('app', 'Status'),
        ];
    }


}
