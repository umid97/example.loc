<?php

namespace app\models;

use Yii;

class OrderItem extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%order_item}}';
    }

    public function rules()
    {
        return [
            [['fullname', 'email', 'user_id', 'phone', 'product_id', 'address', 'product_name', 'sum', 'soni', 'images', 'status'], 'required'],
            [['sum'], 'number'],
            [['soni', 'user_id'], 'integer'],
            [['product_id'], 'integer'],
            [['status'], 'string'],
            [['fullname', 'email', 'address', 'product_name', 'images'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fullname' => Yii::t('app', 'FISH'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Telefon nomer'),
            'address' => Yii::t('app', 'Manzil'),
            'product_name' => Yii::t('app', 'Mahsulot nomi'),
            'sum' => Yii::t('app', 'Summasi'),
            'soni' => Yii::t('app', 'Mahsulot soni'),
            'images' => Yii::t('app', 'Mahsulot rasmi'),
            'status' => Yii::t('app', 'Holati'),
        ];
    }

}
