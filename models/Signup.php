<?php
namespace app\models;
use Yii;
use yii\base\Model;
class Signup extends Model{
    public $fullname;
    public $adress;
    public $phone;
    public $email;
    public $password;
    public $status;
    public $date;

    public function rules()
    {
        return [
            [['fullname', 'date', 'adress', 'phone', 'email', 'password', 'status'], 'required'],
            [['status'], 'string'],
            [['fullname', 'adress', 'email', 'password'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 50],
            [['email'], 'unique', 'targetClass' => 'app\models\Users'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fullname' => Yii::t('app', 'FISH'),
            'adress' => Yii::t('app', 'Manzil'),
            'phone' => Yii::t('app', 'Telefon nomer'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Parol'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function signup(){
        if($this->validate()){
            $users = new Users();
            $users->attributes = $this->attributes;
            return $users->create();
        }
    }
}