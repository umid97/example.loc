<?php
namespace app\components;
use app\models\Category;
use app\models\Products;
use yii\helpers\Url;

class MenuWidgets extends \yii\base\Widget
{
    public static $menu = [];
    private $products;

    public function __construct(){
        $model = Category::find()
            ->asArray()
            ->indexBy('id')
            ->where(['status' => '1'])
            ->limit(8)
            ->all();
        foreach ($model as $key => $r){
            self::$menu[$key] = $r['name'];
        }
    }

    protected function getShow(){
        $str = '<ul class="ht-dropdown mega-menu-2">';
        foreach (self::$menu as $key => $val){
            $model = Products::find()
                ->asArray()
                ->where(['category_id' => $key])
                ->limit(6)
                ->orderBy(['id' => SORT_DESC])
                ->all();
            $str .= '<li style="min-height: 200px"><a href="'.Url::to(['main/category', 'id' => $key]).'"><h3 style="font-size: 12px;">'.$val.'</h3></a>';
            if(!empty($model)){
                $str .= '<ul>';
                foreach ($model as $r){
                    $str .= '<li><a href="'.Url::to(['main/products', 'id'=>$r['id']]).'">'.$r["name"].'</a></li>';
                }
                $str .= '</ul></li>';
            }
            else{
                $str .= '</li>';
            }
        }
        $str .= '</ul>';
        $this->products = $str;

    }

    public function show(){
        $this->getShow();
        return $this->products;
    }
}