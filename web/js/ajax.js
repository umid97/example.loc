$(function(){

    $('.numbers').click(function(){
        if($(this).val() <= 0){
            $(this).val(1);
        }
    });

    $('.numbers').blur(function(){
        if($(this).val() <= 0){
            $(this).val(1);
        }
    });

    $('#add-to-cart').click(function (e) {
        e.preventDefault();
        var count = $('.cart-plus-minus-box').val();
        var id = $(this).data('id');
        var href = $(this).attr('href');
        $.ajax({
            data: {id: id, son: count},
            type: 'GET',
            url: href,
            success: function(res){
                if(!res) return false;
                $('#loading').html(res);
                var count = $('#total-count').html();
                $('#wish-lists').html(count);

            }
        });
        $('#modal_one').modal('show').find('#loading').load();
    });

    $('#modal_one').on('click', '.remove-product', function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        $.ajax({
            url: href,
            type: 'GET',
            success: function(res){
                if(!res) return false;
                $('#loading').html(res);
                var count = $('#total-count').html();
                $('#wish-lists').html(count);

            }
        });
        $('#modal_one').modal('show').find('#loading').load();
    });


    $('.clicked').click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        $.ajax({
            type: 'GET',
            url: href,
            success: function(res){
                if(!res) return false;
                $('#loading').html(res);
                var count = $('#total-count').html();
                $('#wish-lists').html(count);

            }
        });
        $('#modal_one').modal('show').find('#loading').load();
    });

    $('.modal-one').click(function(e){
        e.preventDefault();
        let id = $(this).data('id');
        $.ajax({
            data: {id: id},
            url: 'main/data-one',
            type: 'GET',
            success: function(res){
                if(!res) return false;
                $('#loading').html(res);
            },
            error: function(){
                console.log('Xatolik mavjud!');
            }
        });

        $('#modal_one').modal('show').find('#loading').load();
    });


    $('#modal_one').on('click', '.images_slider',  function(e){
        e.preventDefault();
        var href = $(this).attr('src');
        $('#big-img').attr('src', href);
    })
})