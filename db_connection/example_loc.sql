-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 03 2020 г., 06:54
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `example.loc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category`
--

CREATE TABLE `shop_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_category`
--

INSERT INTO `shop_category` (`id`, `name`, `keywords`, `description`, `status`) VALUES
(1, 'Kompyuterlar', 'Noutbook, Netbook, Core i3, Core i5', 'Yangi Noutbooklar', '1'),
(2, 'Televizorlar', 'Samsung, Artel, Shiwaki', 'Televizorlar', '1'),
(3, 'Uy jihozlari', 'Uy', 'Uy', '1'),
(4, 'Qurilish materiallari', 'Qurilish', 'Qurilish', '1'),
(5, 'Kitoblar', 'Kitoblar', 'Roman', '1'),
(6, 'Soatlar', 'Soat', 'Soat', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_orders`
--

CREATE TABLE `shop_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sum` float NOT NULL,
  `son` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_orders`
--

INSERT INTO `shop_orders` (`id`, `user_id`, `product_id`, `name`, `sum`, `son`, `images`, `status`) VALUES
(57, 1, 7, 'Soadat asri', 20, 1, '6.jpg', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_item`
--

CREATE TABLE `shop_order_item` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sum` float NOT NULL,
  `soni` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` enum('active','noactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_order_item`
--

INSERT INTO `shop_order_item` (`id`, `fullname`, `user_id`, `email`, `phone`, `address`, `product_name`, `product_id`, `sum`, `soni`, `images`, `status`) VALUES
(1, 'Nasriddinov Umidjon', 1, 'webdevelopers01@mail.ru', '+998974169709', 'Qo\'qon', 'O\'tkan kunlar', 6, 10, 3, '5.jpg', 'active'),
(2, 'Nasriddinov Umidjon', 1, 'webdevelopers01@mail.ru', '+998974169709', 'Qo\'qon', 'Soadat asri', 7, 20, 2, '6.jpg', 'active'),
(3, 'Nasriddinov Umidjon', 1, 'webdevelopers01@mail.ru', '+998974169709', 'Qo\'qon', 'Samsung LG', 8, 250, 2, 'lg.jpg', 'active'),
(4, 'Nasriddinov Umidjon', 1, 'webdevelopers01@mail.ru', '+998974169709', 'Qo\'qon', 'Core i7', 9, 750, 1, '1.jpg', 'active');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_products`
--

CREATE TABLE `shop_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `images` varchar(255) NOT NULL,
  `sum` float NOT NULL,
  `rasprodaj` enum('1','0') NOT NULL,
  `new` enum('1','0') NOT NULL,
  `status` enum('1','0') NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_products`
--

INSERT INTO `shop_products` (`id`, `name`, `content`, `images`, `sum`, `rasprodaj`, `new`, `status`, `keywords`, `description`, `date`, `category_id`) VALUES
(2, 'Core i3', 'Core i3', '1.jpg', 450, '0', '1', '1', 'Core i3', 'Core i3', '2020-04-20', 1),
(3, 'Core i5', 'Core i5', '2.jpg', 550, '0', '0', '1', 'Core i5', 'Core i5', '2020-04-19', 1),
(4, 'Tunika', 'Tunika', '3.jpg', 5, '1', '0', '1', 'Tunika', 'Tunika', '2020-04-12', 4),
(5, 'Sement', 'Sement', '4.jpg', 1, '1', '1', '1', 'Sement', 'Sement', '2020-04-19', 4),
(6, 'O\'tkan kunlar', 'O\'tkan kunlar', '5.jpg', 10, '1', '1', '1', 'Kun', 'Kun', '2020-04-05', 5),
(7, 'Soadat asri', 'Saodat asri', '6.jpg', 20, '1', '1', '1', 'Saodat asri', 'Saodat asri', '2020-04-01', 5),
(8, 'Samsung LG', 'LG', 'lg.jpg', 250, '1', '1', '1', 'LG', 'LG', '2020-01-18', 2),
(9, 'Core i7', 'Core i7', '1.jpg', 750, '0', '1', '1', 'core i7, daniel', 'core i7', '2020-01-18', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_users`
--

CREATE TABLE `shop_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('active','noactive') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_users`
--

INSERT INTO `shop_users` (`id`, `fullname`, `adress`, `phone`, `email`, `password`, `status`, `date`) VALUES
(1, 'Nasriddinov Umidjon', 'Qo\'qon', '+998974169709', 'webdevelopers01@mail.ru', '123', 'active', '2020-12-31'),
(2, 'Karimov Rustam', 'Farg\'ona', '+998974169709', 'admin@mail.ru', '123456', 'active', '2002-11-29'),
(3, 'Sanjar', 'Toshkent', '1234567', 'sanjar@mail.ru', '123456', 'active', '1995-07-30'),
(4, 'Jamshidbek', 'Qo\'qon', '+998974169709', 'jamshid@mail.ru', 'jamshid', 'active', '2004-12-31'),
(5, 'Tolibjonov Doniyorbek Nabijon o\'g\'li', 'Katta Tagob', '+998994311436', 'doniyortolibjonov10@gmail.com', '123456', 'active', '1999-06-09'),
(6, 'Azizbek Karimov ', 'Andijon', '+998945556698', 'aziz@mail.ru', 'aziz', 'active', '2003-01-01');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `shop_category`
--
ALTER TABLE `shop_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_orders`
--
ALTER TABLE `shop_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_order_item`
--
ALTER TABLE `shop_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_products`
--
ALTER TABLE `shop_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `shop_users`
--
ALTER TABLE `shop_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `shop_category`
--
ALTER TABLE `shop_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `shop_orders`
--
ALTER TABLE `shop_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT для таблицы `shop_order_item`
--
ALTER TABLE `shop_order_item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `shop_products`
--
ALTER TABLE `shop_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `shop_users`
--
ALTER TABLE `shop_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `shop_products`
--
ALTER TABLE `shop_products`
  ADD CONSTRAINT `shop_products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `shop_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
