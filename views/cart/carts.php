<?php
$count = 0; $sum = 0;
use yii\helpers\Url;
?>
<div class="cart-main-area pb-100">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>Xarid savatchasi</h2>
            <?php if(Yii::$app->session->hasFlash('success')): ?>
                <p class="alert alert-warning">
                    <?=Yii::$app->session->getFlash('success'); ?>
                </p>
            <?php endif; ?>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <?php if(!empty($model)): ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Form Start -->
                <form action="#">
                    <!-- Table Content Start -->
                    <div class="table-content table-responsive mb-50">
                        <table>
                            <thead>
                            <tr>
                                <th class="product-thumbnail">Rasm</th>
                                <th class="product-name">Maxsulot nomi</th>
                                <th class="product-price">Summasi</th>
                                <th class="product-quantity">Soni</th>
                                <th class="product-subtotal">Umumiy summasi</th>
                                <th class="product-remove">O'chirish</th>
                                <th class="product-price">Buyurtma berish</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($model as $r): ?>
                                    <?php
                                    $count += $r['son'];
                                    $sum += $r['son'] * $r['sum'];
                                    ?>
                                    <tr>
                                        <td class="product-thumbnail">
                                            <a href="#"><img src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="cart-image" /></a>
                                        </td>
                                        <td class="product-name"><a href="#"><?=$r['name']?></a></td>
                                        <td class="product-price"><span class="amount">$<?=$r['sum']?></span></td>
                                        <td class="product-quantity"><input type="number" value="<?=$r['son']?>" /></td>
                                        <td class="product-subtotal">$<?=$r['son'] * $r['sum']?></td>
                                        <td class="product-remove"> <a href="<?=Url::to(['cart/del-items', 'id' => $r['id']])?>"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                        <td class="product-price">
                                            <a href="<?=Url::to(['cart/oformit', 'id' => $r['id']])?>" class="btn btn-info btn-sm"><i class="fa fa-plus"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- Table Content Start -->
                    <div class="row">
                        <!-- Cart Button Start -->
                        <div class="col-md-8 col-sm-7 col-xs-12">
                            <div class="buttons-cart">
                                <a href="<?=Url::to(['cart/save-all'])?>">Jami Mahsulotlarga Zakaz berish</a>
                            </div>
                        </div>
                        <!-- Cart Button Start -->
                        <!-- Cart Totals Start -->
                        <div class="col-md-4 col-sm-5 col-xs-12">
                            <div class="cart_totals">
                                <h2>Jami Mahsulot</h2>
                                <br />
                                <table>
                                    <tbody>
                                    <tr class="cart-subtotal">
                                        <th>Jami Soni</th>
                                        <td><span class="amount" id="total-count"><?=$_SESSION['cart.son'] ?? $count; ?></span></td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Jami Summasi</th>
                                        <td>
                                            <strong><span class="amount">$<?=$_SESSION['cart.sum'] ?? $sum; ?></span></strong>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Cart Totals End -->
                    </div>
                    <!-- Row End -->
                </form>
                <!-- Form End -->
            </div>
            <?php else: ?>
                <div class="col-xs-12">
                    <p class="alert alert-danger">
                        Xarid savatchasi bo'sh!
                    </p>
                </div>
            <?php endif; ?>
        </div>
        <!-- Row End -->
    </div>
</div>