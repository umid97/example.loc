<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<div class="cart-main-area pb-100">
    <div class="container">
    <div class="section-title mb-50">
        <h2>Buyurtma berish</h2>
    </div>
    <div class="row">
        <?php if(Yii::$app->session->hasFlash('error')): ?>
            <p class="alert alert-warning">
                <?=Yii::$app->session->getFlash('error'); ?>
            </p>
        <?php endif; ?>
        <div class="col-sm-12">
            <div class="table-content table-responsive mb-50">
                <table>
                    <thead>
                    <tr>
                        <th class="product-thumbnail">Rasm</th>
                        <th class="product-name">Maxsulot nomi</th>
                        <th class="product-price">Summasi</th>
                        <th class="product-quantity">Soni</th>
                        <th class="product-price">Umumiy summasi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="product-thumbnail">
                                <a href="#"><img src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="cart-image" /></a>
                            </td>
                            <td class="product-name"><a href="#"><?=$order->name?></a></td>
                            <td class="product-price"><span class="amount">$<?=$order->sum?></span></td>
                            <td class="product-quantity"><input type="number" value="<?=$order->son?>" /></td>
                            <td class="product-subtotal">$<?=$order->son * $order->sum?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12">
            <?php
                $f = \yii\bootstrap\ActiveForm::begin();
                echo $f->field($model, 'fullname')->textInput(['value' => Yii::$app->user->identity->fullname]);
                echo $f->field($model, 'email')->textInput(['value' => Yii::$app->user->identity->email]);
                echo $f->field($model, 'phone')->textInput(['value' => Yii::$app->user->identity->phone]);
                echo $f->field($model, 'address')->textInput(['value' => Yii::$app->user->identity->adress]);
            echo $f->field($model, 'soni')->input('number', ['value' => $order->son, 'class' => 'numbers form-control']);
                echo $f->field($model, 'product_name')->textInput(['value' => $order->name, 'readonly' => true]);
                echo $f->field($model, 'sum')->textInput(['value' => $order->sum, 'readonly' => true]);
                echo $f->field($model, 'images')->textInput(['value' => $order->images, 'readonly' => true]);
                echo Html::submitButton('Buyurtma berish', ['class' => 'btn btn-info btn-sm']);

                \yii\bootstrap\ActiveForm::end();
            ?>
        </div>
    </div>
</div>
</div>