<?php
$count = 0; $sum = 0;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="cart-main-area pb-100">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>Buyurtma berish</h2>
            <?php
                $f = \yii\bootstrap\ActiveForm::begin();
            echo $f->field($model, 'fullname')->textInput(['value' => Yii::$app->user->identity->fullname]);
            echo $f->field($model, 'email')->textInput(['value' => Yii::$app->user->identity->email]);
            echo $f->field($model, 'phone')->textInput(['value' => Yii::$app->user->identity->phone]);
            echo $f->field($model, 'address')->textInput(['value' => Yii::$app->user->identity->adress]);
            echo Html::submitButton('Buyurtma berish', ['class' => 'btn btn-info btn-sm']);



            \yii\bootstrap\ActiveForm::end();
            ?>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- Form Start -->
                    <form action="#">
                        <!-- Table Content Start -->
                        <div class="table-content table-responsive mb-50">
                            <table>
                                <thead>
                                <tr>
                                    <th class="product-thumbnail">Rasm</th>
                                    <th class="product-name">Maxsulot nomi</th>
                                    <th class="product-price">Summasi</th>
                                    <th class="product-quantity">Soni</th>
                                    <th class="product-subtotal">Umumiy summasi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($orders as $r): ?>
                                    <?php
                                    $count += $r['son'];
                                    $sum += $r['son'] * $r['sum'];
                                    ?>
                                    <tr>
                                        <td class="product-thumbnail">
                                            <a href="#"><img src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="cart-image" /></a>
                                        </td>
                                        <td class="product-name"><a href="#"><?=$r['name']?></a></td>
                                        <td class="product-price"><span class="amount">$<?=$r['sum']?></span></td>
                                        <td class="product-quantity"><input type="number" value="<?=$r['son']?>" /></td>
                                        <td class="product-subtotal">$<?=$r['son'] * $r['sum']?></td>

                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- Table Content Start -->
                        <div class="row">
                        </div>
                        <!-- Row End -->
                    </form>
                    <!-- Form End -->
                </div>
        </div>
        <!-- Row End -->
    </div>
</div>