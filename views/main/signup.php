<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
?>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb" style="background: rgba(0, 0, 0, 0) url(<?=Url::base().'/img/blog/5.png'?>) no-repeat scroll center center / cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="<?=Url::home()?>">home</a></li>

                        <?php if(Yii::$app->session->hasFlash('error')): ?>
                            <hr><li><a><?=Yii::$app->session->getFlash('error'); ?></a></li>
                            <hr>
                        <?php endif; ?>
                        <li><a href="#">contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->
<!-- Contact Email Area Start -->
<div class="contact-email-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="mb-5">Contact Us</h3>
                <div class="row">
                    <?php
                        $f = ActiveForm::begin([
                            'options' => [
                                'id' => 'contact-form',
                                'class' => 'contact-form'
                            ],
                            'enableAjaxValidation' => true
                        ]);
                    ?>
                        <div class="address-wrapper">
                            <div class="col-md-12">
                                <div class="address-fname">
                                    <?=$f->field($model, 'fullname')?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="address-email">
                                    <?=$f->field($model, 'adress');?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="address-web">
                                    <?=$f->field($model, 'phone')?>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="address-subject">
                                    <?=$f->field($model, 'date')->input('date'); ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <?=$f->field($model, 'email');?>
                            </div>
                            <div class="col-xs-6">
                                <?=$f->field($model, 'password')->passwordInput();?>
                            </div>

                        </div>
                        <p class="form-message ml-15"></p>
                        <div class="col-xs-12 footer-content mail-content">
                            <div class="send-email pull-right">
                                <?=Html::submitButton('Ma\'lumotlarni saqlash', ['class' => 'btn btn-info btn-sm'])?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Email Area End -->