<?php
    use yii\helpers\Url;
?>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100" style="background: rgba(0, 0, 0, 0) url(<?=Url::base().'/img/blog/5.png'?>) no-repeat scroll center center / cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <h1><?=$model->name?></h1>
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="<?=Url::home()?>">bosh sahifa</a></li>
                        <li><a href="#"><?=$model->name; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Breadcrumb End -->
<!-- Product Thumbnail Start -->
<div class="main-product-thumbnail pb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <img id="big-img" src="<?=Url::base()?>/img/new-products/1_1.jpg" data-zoom-image="<?=Url::base()?>/img/new-products/1_1.jpg" alt="product-image" />

                <div id="small-img" class="mt-20">

                    <div class="thumb-menu owl-carousel">
                        <a href="#" data-image="<?=Url::base()?>/img/new-products/1_2.jpg" data-zoom-image="<?=Url::base()?>/img/new-products/1_2.jpg">
                            <img src="<?=Url::base()?>/img/new-products/1_2.jpg" alt="product-image" />
                        </a>

                        <a href="#" data-image="<?=Url::base()?>/img/new-products/2_1.jpg" data-zoom-image="<?=Url::base()?>/img/new-products/2_1.jpg">
                            <img src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="product-image" />
                        </a>

                        <a href="#" data-image="<?=Url::base()?>/img/new-products/2_2.jpg" data-zoom-image="<?=Url::base()?>/img/new-products/2_2.jpg">
                            <img src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="product-image" />
                        </a>

                        <a href="#" data-image="<?=Url::base()?>/img/new-products/3_1.jpg" data-zoom-image="<?=Url::base()?>/img/new-products/3_1.jpg">
                            <img src="<?=Url::base()?>/img/new-products/3_1.jpg" alt="product-image" />
                        </a>

                        <a href="#" data-image="<?=Url::base()?>/img/new-products/2_1.jpg" data-zoom-image="<?=Url::base()?>/img/new-products/2_1.jpg">
                            <img src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="product-image" />
                        </a>
                    </div>
                </div>
            </div>
            <!-- Thumbnail Description Start -->
            <div class="col-sm-7">
                <div class="thubnail-desc fix">
                    <h2 class="product-header"><?=$model->name?></h2>
                    <!-- Product Rating Start -->
                    <div class="rating-summary fix mtb-20">
                        <div class="rating f-left mr-10">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <div class="rating-feedback f-left">
                            <a href="#">0 reviews</a> /
                            <a href="#">Write a review</a>
                        </div>
                    </div>
                    <!-- Product Rating End -->
                    <!-- Product Price Start -->
                    <div class="pro-price mb-20">
                        <ul class="pro-price-list">
                            <li class="price">$<?=$model->sum?></li>
                        </ul>
                    </div>
                    <!-- Product Price End -->
                    <!-- Product Price Description Start -->
                    <div class="product-price-desc">
                        <ul class="pro-desc-list">
                            <li>Product Code: <span>SAMI</span></li>
                            <li>Reward Points: <span>1000</span></li>
                            <li>Availability: <span>in Stock</span></li>
                        </ul>
                    </div>
                    <!-- Product Price Description End -->
                    <!-- Product Box Quantity Start -->
                    <div class="box-quantity mtb-20">
                        <div class="quantity-item">
                            <label>Qty: </label>
                            <div class="cart-plus-minus">
                                <input class="cart-plus-minus-box" type="text" name="qtybutton" value="1">
                            </div>
                        </div>
                    </div>
                    <!-- Product Box Quantity End -->
                    <!-- Product Button Actions Start -->
                    <div class="product-button-actions">
                        <button href="<?=Url::to(['cart/add-cart'])?>" data-id="<?=$model->id; ?>" class="add-to-cart" id="add-to-cart">Add-to-cart</button>
                        <a href="wish-list.html" data-toggle="tooltip" title="Add to Wishlist" class="same-btn mr-15"><i class="pe-7s-like"></i></a>
                        <button data-toggle="tooltip" title="Compare this Product" class="same-btn"><i class="pe-7s-repeat"></i></button>
                    </div>
                    <!-- Product Button Actions End -->
                    <!-- Product Social Link Share Start -->
                    <div class="social-shared">
                        <ul>
                            <li class="f-book">
                                <a href="#">
                                    <span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                    <span>like</span>
                                    <span>1</span>
                                </a>
                            </li>
                            <li class="twitter">
                                <a href="#">
                                    <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                    <span>tweet</span>
                                </a>
                            </li>
                            <li class="pinterest">
                                <a href="#">
                                    <span><i class="fa fa-google" aria-hidden="true"></i></span>
                                    <span>plus</span>
                                </a>
                            </li>
                            <!-- Product Social Link Share Dropdown Start -->
                            <li class="share-post">
                                <a href="#">
                                    <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                                    <span>share</span>
                                </a>
                                <ul class="sharable-dropdown">
                                    <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i>facebook</a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>twitter</a></li>
                                    <li><a href="#"><i class="fa fa-print" aria-hidden="true"></i>print</a></li>
                                    <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>email</a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-square" aria-hidden="true"></i>pinterest</a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i>google+</a></li>
                                    <li><a href="#"><i class="fa fa-plus-square" aria-hidden="true"></i>more(99)</a></li>
                                </ul>
                            </li>
                            <!-- Product Social Link Share Dropdown End -->
                        </ul>
                        <hr>
                        <p><?=$model->content?></p>
                    </div>
                    <!-- Product Social Link Share End -->
                </div>
            </div>
            <!-- Thumbnail Description End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Product Thumbnail End -->