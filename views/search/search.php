<?php
use yii\helpers\Url;
?>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100" style="background: rgba(0, 0, 0, 0) url(<?=Url::base().'/img/blog/5.png'?>) no-repeat scroll center center / cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <h1><?=$search?></h1>
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="<?=Url::home()?>">bosh sahifa</a></li>
                        <li><a href="#"><?=$search; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Breadcrumb End -->
<?php if(!empty($model)): ?>
<div class="all-categories pb-100">
    <div class="container">
        <div class="row">
            <!-- Sidebar Content Start -->
            <div class="col-md-12 col-md-push-0">
                <!-- Best Seller Product Start -->
                <div class="best-seller">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tab-content categorie-list ">
                                <!-- #list-view End -->
                                <div id="grid-view" class="tab-pane fade in active mt-40">
                                    <div class="row">
                                        <?php foreach ($model as $k => $v): ?>
                                            <div class="col-md-3 col-sm-6">
                                                <!-- Single Product Start -->
                                                <div class="single-product">
                                                    <!-- Product Image Start -->
                                                    <div class="pro-img">
                                                        <a href="product-page.html">
                                                            <img class="primary-img" src="<?=Url::base()?>/img/new-products/1_1.jpg" alt="single-product">
                                                            <img class="secondary-img" src="<?=Url::base()?>/img/new-products/1_2.jpg" alt="single-product">
                                                        </a>
                                                        <div class="quick-view">
                                                            <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                                        </div>
                                                        <?php if($v['new']): ?>
                                                            <span class="sticker-new">new</span>
                                                        <?php else: ?>

                                                        <?php endif; ?>
                                                    </div>
                                                    <!-- Product Image End -->
                                                    <!-- Product Content Start -->
                                                    <div class="pro-content text-center">
                                                        <h4><a href="product-page.html"><?=$v['name']?></a></h4>
                                                        <p class="price"><span>$241.99</span></p>
                                                        <div class="action-links2">
                                                            <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                                        </div>
                                                    </div>
                                                    <!-- Product Content End -->
                                                </div>
                                                <!-- Single Product End -->
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <!-- Row End -->
                                    <div class="row mt-40 mb-70">
                                        <div class="col-sm-6">
                                            <?php
                                            echo \yii\widgets\LinkPager::widget([
                                                'pagination' => $page
                                            ])
                                            ?>
                                        </div>
                                    </div>
                                    <!-- Row End -->
                                </div>
                                <!-- #Grid-view End -->
                            </div>
                            <!-- .Tab Content End -->
                        </div>
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Best Seller Product End -->
            </div>
            <!-- Sidebar Content End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<?php else: ?>
    <div class="container">
        <h5 class="alert alert-danger">
            Bunday mahsulot topilmadi!
        </h5>
    </div>
<?php endif; ?>
