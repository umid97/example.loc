<?php
namespace app\assets;
use yii\web\AssetBundle;

class ModeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/vendor/modernizr-2.8.3.min.js',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}